const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass')); // Specify the 'sass' compiler

function compileSass() {
  return gulp
    .src('assets/scss/**/*.scss') // Source SCSS files
    .pipe(sass().on('error', sass.logError)) // Compile SCSS to CSS
    .pipe(gulp.dest('assets/css')); // Output directory for CSS
}

function watch() {
  gulp.watch('assets/scss/**/*.scss', compileSass); // Watch for changes in SCSS files
}

exports.compileSass = compileSass;
exports.watch = watch;

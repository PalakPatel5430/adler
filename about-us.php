<?php
// Include the header.php file
include('templates/header.php');
?>
<section class="inner-banner" style="background-image: url('assets/images/about-us-inner-banner.jpg');">
    <div class="inner-banner-info">
        <h1 class="inner-banner-caption text-white">About</h1>
        <div class="breadcrumbs">
            <ul>
                <li><a href="#"><span class="border-white"></span>Home</a></li>
                <li>About</li>
            </ul>
        </div>
    </div>
</section>
<section class="inner-page about-us-wrap">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xl-6 order-2 order-xl-1">
                <div class="about-us-info pt-4 pt-xl-0">
                    <p>Leveraging the unstoppable prowess of Adler Talent Solutions, one of India's top most Strategic HR Company, we are poised to revolutionize the HR landscape like never before. Armed with unrivalled expertise in HR services, cutting-edge HR technology, and comprehensive HR mentoring, we are committed to unveiling the perfect HR solution for your organization. Allow us to manage all your mundane HR tasks with unparalleled precision seamlessly.</p>
                    <p>We roar with passion and commitment to transform HR practices. We optimize HR processes by leveraging best practices to enhance efficiency and effectiveness. Together, we achieve success by aligning our solutions with your strategic goals and maximizing your organization's potential. Striving to revolutionize the HR landscape, constantly pushing boundaries and introducing innovative approaches.</p>
                </div>
                <div class="achievement-grid pt-5">
                    <div class="inner-col">
                        <div class="digit-wrap">
                            <h6 class="digit">30+</h6>
                            <p>HR Services offered</p>
                        </div>
                    </div>
                    <div class="inner-col">
                        <div class="digit-wrap">
                            <h6 class="digit">3200+</h6>
                            <p>Employees Managed</p>
                        </div>
                    </div>
                    <div class="inner-col">
                        <div class="digit-wrap">
                            <h6 class="digit">11</h6>
                            <p>Domestic Geographies</p>
                        </div>
                    </div>
                    <div class="inner-col">
                        <div class="digit-wrap">
                            <h6 class="digit">3</h6>
                            <p>International Geographies</p>
                        </div>
                    </div>
                    <div class="inner-col">
                        <div class="digit-wrap">
                            <h6 class="digit">7</h6>
                            <p>Mentors</p>
                        </div>
                    </div>
                    <div class="inner-col">
                        <div class="digit-wrap">
                            <h6 class="digit">10</h6>
                            <p>Tech Partners</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xl-6 order-1 order-xl-2">
                <img class="mw-sm-100" src="assets/images/about-welcome.jpg" alt="About Us" />
            </div>
        </div>
        <div class="mission">
            <div class="d-flex">
                <div>
                    <h2 class="section-title pb-3">Mission</h2>
                    <h6 class="font-30 fw-normal pb-3">Our mission, <span class="text-primary">HEART,</span> is to <span class="text-primary">H</span>arness, <span class="text-primary">E</span>mpower, <span class="text-primary">A</span>dapt, <span class="text-primary">R</span>espond, and <span class="text-primary">T</span>ransform organizations through Customized HR Solutions.</h6>
                    <h6 class="font-30 fw-normal">With <span class="text-primary">HEART</span> as our guiding principle, we are committed to creating a lasting impact, ensuring that your HR journey is filled with passion, purpose, and remarkable results.</h6>
                </div>
                <div class="mission-ball">
                    <img src="assets/images/mission-ball.svg" alt="Mission" />
                </div>
            </div>
        </div>
    </div>
    <div class="about-team">
        <div class="team-dots-pattern">
            <img src="assets/images/dots-pattern.png" alt="Team">
        </div>
        <div class="container">
            <h2 class="section-title pb-2">Team</h2>
            <p>With our expertise and strategic approach, we build strong foundations for your team,<br /> empowering them to thrive and achieve remarkable results</p>
            <div class="about-team-slider common-card">
                <div class="card">
                    <img class="w-100 radius-8" src="assets/images/team-member-1.jpg" alt="Team" />
                    <div class="card-info-wrap">
                        <div class="card-info">
                            <p class="text-uppercase m-0 font-12">HEAD OF CONSULTING</p>
                            <h3 class="font-30">Thomas Novak</h3>
                        </div>
                        <hr />
                        <a href="#" class="btn btn-link p-0 text-decoration-none text-capitalize"><span>Linkdin</span> <em class="icon icon-up-arrow"></em></a>
                    </div>
                </div>
                <div class="card">
                    <img class="w-100 radius-8" src="assets/images/team-member-2.jpg" alt="Team" />
                    <div class="card-info-wrap">
                        <div class="card-info">
                            <p class="text-uppercase m-0 font-12">MANAGING DIRECTOR</p>
                            <h3 class="font-30">Amanda Berry</h3>
                        </div>
                        <hr />
                        <a href="#" class="btn btn-link p-0 text-decoration-none text-capitalize"><span>Linkdin</span> <em class="icon icon-up-arrow"></em></a>
                    </div>
                </div>
                <div class="card">
                    <img class="w-100 radius-8" src="assets/images/team-member-3.jpg" alt="Team" />
                    <div class="card-info-wrap">
                        <div class="card-info">
                            <p class="text-uppercase m-0 font-12">INVESTMENT EXPERT</p>
                            <h3 class="font-30">Edward Willey</h3>
                        </div>
                        <hr />
                        <a href="#" class="btn btn-link p-0 text-decoration-none text-capitalize"><span>Linkdin</span> <em class="icon icon-up-arrow"></em></a>
                    </div>
                </div>
                <div class="card">
                    <img class="w-100 radius-8" src="assets/images/team-member-4.jpg" alt="Team" />
                    <div class="card-info-wrap">
                        <div class="card-info">
                            <p class="text-uppercase m-0 font-12">HEAD OF CONSULTING</p>
                            <h3 class="font-30">Joan Emma</h3>
                        </div>
                        <hr />
                        <a href="#" class="btn btn-link p-0 text-decoration-none text-capitalize"><span>Linkdin</span> <em class="icon icon-up-arrow"></em></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="core-values">
        <div class="container">
            <div class="d-flex align-items-center pb-5">
                <h2 class="section-title m-0 pe-4"> Core Values </h2>
                <img src="assets/images/core-values.svg" alt="Core Values" />
            </div>
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="d-flex align-items-center justify-content-between core-values-box">
                        <h3 class="font-30 m-0 text-white">Integerity</h3>
                        <img src="assets/images/integerity.svg" alt="Integerity" />
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="d-flex align-items-center justify-content-between core-values-box">
                        <h3 class="font-30 m-0 text-white">Passion for Excellence</h3>
                        <img src="assets/images/core-value-2.svg" alt="Passion for Excellence" />
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="d-flex align-items-center justify-content-between core-values-box">
                        <h3 class="font-30 m-0 text-white">Team Work</h3>
                        <img src="assets/images/core-value-3.svg" alt="Team Work" />
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="d-flex align-items-center justify-content-between core-values-box">
                        <h3 class="font-30 m-0 text-white">Respect & Humility</h3>
                        <img src="assets/images/core-value-4.svg" alt="Respect & Humility" />
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="d-flex align-items-center justify-content-between core-values-box">
                        <h3 class="font-30 m-0 text-white">Joyous Work Atmosphere</h3>
                        <img src="assets/images/core-value-5.svg" alt="Joyous Work Atmosphere" />
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="d-flex align-items-center justify-content-between core-values-box">
                        <h3 class="font-30 m-0 text-white">Constant Innovation</h3>
                        <img src="assets/images/core-value-6.svg" alt="Constant Innovation" />
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="d-flex align-items-center justify-content-between core-values-box">
                        <h3 class="font-30 m-0 text-white">Functional Competence</h3>
                        <img src="assets/images/core-value-7.svg" alt="Functional Competence" />
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="d-flex align-items-center justify-content-between core-values-box">
                        <h3 class="font-30 m-0 text-white">Data-Driven Approach</h3>
                        <img src="assets/images/core-value-8.svg" alt="Data-Driven Approach" />
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="d-flex align-items-center justify-content-between core-values-box">
                        <h3 class="font-30 m-0 text-white">Transparency</h3>
                        <img src="assets/images/core-value-9.svg" alt="Transparency" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="awards">
        <div class="container">
            <h2 class="section-title"> Awards & Certifications </h2>
            <div class="awards-img-wrap">
                <div class="row">
                    <div class="col-sm-6 col-lg-4">
                        <img class="w-100" src="assets/images/awards-1.jpg" alt="Awards & Certifications" />
                    </div>
                    <div class="col-sm-6 col-lg-4">
                        <img class="w-100" src="assets/images/awards-2.jpg" alt="Awards & Certifications" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
// Include the header.php file
include('templates/footer.php');
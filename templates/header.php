<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="assets/css/styles.css">
        <title>Adler</title>
        <link rel="apple-touch-icon" sizes="32x32" href="assets/images/favicon-96x96.png" />
        <link rel="apple-touch-icon" sizes="32x32" href="assets/images/favicon-32x32.png" />
        <link rel="apple-touch-icon" sizes="32x32" href="assets/images/favicon-16x16.png" />
        <link rel="apple-touch-icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <!-- fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Sora:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
        <!-- Jquery -->
        <script src="assets/js/jquery.js"></script>
    </head>

    <body>
        <header class="header">
            <div class="container-fluid">
                <nav class="navbar-wrap">
                    <div class="logo">
                        <a href="#"><img src="assets/images/logo.svg" alt="Think HR" /></a>
                    </div>
                    <div class="navbar-links">
                        <label for="drop" class="toggle"><em class="icon-menu-icon"></em></label>
                        <input class="d-none" type="checkbox" id="drop" />
                        <ul class="menu">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">About Us</a></li>
                            <li>
                                <!-- First Tier Drop Down -->
                                <label for="drop-2" class="toggle">Solutions<em class="icon icon-plus"></em></label>
                                <a href="#">Solutions</a>
                                <input type="checkbox" id="drop-2" />
                                <ul>
                                    <li>
                                        <!-- Second Tier Drop Down -->
                                        <label for="drop-3" class="toggle">Services<em class="icon icon-plus"></em></label>
                                        <a href="#">Services</a>
                                        <input type="checkbox" id="drop-3" />
                                        <ul>
                                            <li><a href="#">HR Strategy</a></li>
                                            <li><a href="#">Payroll and Compliances</a></li>
                                            <li><a href="#">HR Operations</a></li>
                                            <li><a href="#">Employee Growth &amp; Learning</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Technology</a></li>
                                    <li><a href="#">Mentors </a></li>
                                </ul>
                            </li>
                            <li><a href="#">Why Us</a></li>
                            <li><a href="#">Blogs</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>

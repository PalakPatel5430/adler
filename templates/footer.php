<footer class="footer">
    <div class="container-fluid">
        <div class="footer-top">
            <div class="footer-logo">
                <a href="#"><img src="assets/images/footer-logo.svg" alt="Think HR" /></a>
            </div>
            <div class="row">
                <div class="col-sm-4 col-lg-2">
                    <div class="empowering-work">
                        <hr />
                        <h4 class="text-white line-height-lg">Empowering Workforces</h4>
                    </div>
                </div>
                <div class="col-sm-8 col-lg-5">
                    <div class="d-flex quick-links-wrap">
                        <ul class="list-unstyled quick-links">
                            <li><a class="active" href="#">Solutions</a></li>
                            <li><a href="#">Pricing</a></li>
                            <li><a href="#">Meet Our Team</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">HR Guides</a></li>
                            <li><a href="#">Outsourcing</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                        </ul>
                        <ul class="list-unstyled quick-links">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Why Us</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-5">
                    <h6 class="text-white pb-3">Head Office</h6>
                    <h6 class="text-white fw-normal pb-4 line-height-lg">Block A, 1303-1304, 13th Floor, Prahladnagar Trade Centre (PNTC), B/H Titanium City Centre, Times of India Road, Prahladnagar, Ahmedabad - 380015</h6>
                    <div>
                        <a class="text-decoration-none text-white contact-num" href="tel:+918487050658">+91 8487050658</a>
                    </div>
                    <div class="pt-1">
                        <a class="text-decoration-none text-white contact-num" href="tel:+917048855506">+91 7048855506</a>
                    </div>
                    <div class="d-flex align-items-center pt-4">
                        <h6 class="text-white m-0 pe-3">Social Media</h6>
                        <ul class="list-unstyled socail-media-links m-0">
                            <a href="#" class="text-decoration-none"><em class="icon icon-linkdin"></em></a>
                            <a href="#" class="text-decoration-none"><em class="icon icon-facebook"></em></a>
                            <a href="#" class="text-decoration-none"><em class="icon icon-instagram"></em></a>
                            <a href="#" class="text-decoration-none"><em class="icon icon-twitter"></em></a>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="d-sm-flex justify-content-between">
                <p class="text-white m-0">Designed by The Grey Metaphor</p>
                <div class="back-top-top">
                    <a href="#" class="text-white text-decoration-none d-flex align-items-center">Back to Top<em class="icon-back-to-top ps-2"></em></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="node_modules/slick-slider/slick/slick.js"></script>
<script src="assets/js/custom.js"></script>
</body>

</html>

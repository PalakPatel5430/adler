<section class="contact-us-form-wrap">
    <div class="container">
        <h2 class="text-center contact-title">We love to hear from you</h2>
        <form class="contact-us-form">
            <div class="row">
                <div class="col-12 col-md-6">
                    <input type="text" class="form-control" id="name" placeholder="Name*">
                </div>
                <div class="col-12 col-md-6">
                    <input type="email" class="form-control" id="email" placeholder="Email*">
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <input type="text" class="form-control" id="subject" placeholder="Subject">
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <textarea class="form-control" id="message" rows="3" placeholder="Message"></textarea>
                </div>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary">Submit<em class="icon icon-right-arrow"></em></button>
            </div>
        </form>
    </div>
</section>

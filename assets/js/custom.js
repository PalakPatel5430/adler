// Sticky header
$(window).scroll(function () {
  if ($(this).scrollTop() > 120) {
    $('.header').addClass('fixed');
  } else {
    $('.header').removeClass('fixed');
  }
});
$(window).scroll(function () {
  if ($(this).scrollTop() > 300) {
    $('.footer-bottom').addClass('fixed');
  } else {
    $('.footer-bottom').removeClass('fixed');
  }
});

$(document).ready(function () {
  var video1 = document.getElementById('ban_video');
  video1.currentTime = 0;
  $('.mute-bt').click(function () {
    if ($(this).hasClass('stop')) {
      var ban_video = document.getElementById('ban_video');
      $('#ban_video').prop('muted', false);
      $(this).removeClass('stop');
    } else {
      var ban_video = document.getElementById('ban_video');
      $('#ban_video').prop('muted', true);
      $(this).addClass('stop');
    }
  });

  $('.play-bt').click(function () {
    $('.play-bt').hide();
    $('.pause-bt').show();
    var ban_video = document.getElementById('ban_video');
    if ($('.stop-bt').hasClass('active')) {
      ban_video.currentTime = 0;
    }
    ban_video.play();
  });
  $('.pause-bt').click(function () {
    $('.play-bt').show();
    $('.pause-bt').hide();
    $('.pause-bt').addClass('active');
    $('.stop-bt').removeClass('active');
    var ban_video = document.getElementById('ban_video');
    ban_video.pause();
  });
  $('.stop-bt').click(function () {
    $('.play-bt').show();
    $('.pause-bt').hide();
    $('.pause-bt').removeClass('active');
    $('.stop-bt').addClass('active');
    var ban_video = document.getElementById('ban_video');
    ban_video.currentTime = 0;
    ban_video.pause();
  });
});

// The Core of Think HR slider
$('.think-hr-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        arrow: false,
        dots: true,
      },
    },
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
        autoplay: true,
      },
    },
  ],
});
$('.technology-partners-slider').slick({
  infinite: true,
  slidesToShow: 4,
  arrow: true,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
      },
    },
  ],
});
$('.client-testimonial-slider').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  dots: true,
  arrow: false,
  fade: true,
});

$(document).ready(function () {
  var video1 = document.getElementById('testimonailVideo');
  video1.currentTime = 0;
  $('.mute-bt').click(function () {
    if ($(this).hasClass('stop')) {
      var testimonailVideo = document.getElementById('testimonailVideo');
      $('#testimonailVideo').prop('muted', false);
      $(this).removeClass('stop');
    } else {
      var testimonailVideo = document.getElementById('testimonailVideo');
      $('#testimonailVideo').prop('muted', true);
      $(this).addClass('stop');
    }
  });

  $('.play-bt').click(function () {
    $('.play-bt').hide();
    $('.pause-bt').show();
    var testimonailVideo = document.getElementById('testimonailVideo');
    if ($('.stop-bt').hasClass('active')) {
      testimonailVideo.currentTime = 0;
    }
    testimonailVideo.play();
  });
  $('.pause-bt').click(function () {
    $('.play-bt').show();
    $('.pause-bt').hide();
    $('.pause-bt').addClass('active');
    $('.stop-bt').removeClass('active');
    var testimonailVideo = document.getElementById('testimonailVideo');
    testimonailVideo.pause();
  });
  $('.stop-bt').click(function () {
    $('.play-bt').show();
    $('.pause-bt').hide();
    $('.pause-bt').removeClass('active');
    $('.stop-bt').addClass('active');
    var testimonailVideo = document.getElementById('testimonailVideo');
    testimonailVideo.currentTime = 0;
    testimonailVideo.pause();
  });
});

$('.blogs-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
      },
    },
  ],
});
$('.about-team-slider').slick({
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
      },
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
      },
    },
  ],
});

$('.benefits-technology-partners-slider').slick({
  infinite: true,
  slidesToShow: 4,
  arrow: true,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
      },
    },
  ],
});

$('.employee-testimonial-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
      },
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
      },
    },
  ],
});

<?php
include('templates/header.php');
?>
<section class="inner-banner" style="background-image: url('assets/images/technology/technology.jpg');">
    <div class="inner-banner-info">
        <h2 class="text-white">Streamline HR Operations with <br /> optimal technology </h2>
        <h1 class="inner-banner-caption text-white">Technology</h1>
    </div>
</section>
<section class="inner-page technology-wrap">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xl-6 order-2 order-xl-1">
                <div class="welcome-info pt-4 pt-xl-0">
                    <p>The rapid and frequent advancements in technology have compelled nearly every organization to undergo some form of transformation, affecting various aspects of the business. Within the realm of HR, this digital transformation takes on a dual nature: transactional and transformational. Transactional aspects encompass tasks such as payroll, leave management, benefits administration, and time management. On the other hand, the transformational dimension encompasses talent management, focusing on strategic initiatives to cultivate and harness organizational talent.</p>
                </div>
                <h2 class="section-title">Benefits<span class="d-block title-border"></span></h2>
                <div class="benefits-list">
                    <div class="d-flex align-items-center mb-4 pb-2">
                        <img src="assets/images/technology/benefits-1.svg" alt="Benefits" />
                        <h4 class="m-0 ps-4 benefits-title">Fostered talent retention</h4>
                    </div>
                    <div class="d-flex align-items-center mb-4 pb-2">
                        <img src="assets/images/technology/benefits-2.svg" alt="Benefits" />
                        <h4 class="m-0 ps-4 benefits-title">Aligned company strategy and people management</h4>
                    </div>
                    <div class="d-flex align-items-center mb-4 pb-2">
                        <img src="assets/images/technology/benefits-3.svg" alt="Benefits" />
                        <h4 class="m-0 ps-4 benefits-title">Fostered talent retention</h4>
                    </div>
                    <div class="d-flex align-items-center mb-4 pb-2">
                        <img src="assets/images/technology/benefits-4.svg" alt="Benefits" />
                        <h4 class="m-0 ps-4 benefits-title">Boosted employee satisfaction Reduced HR costs through streamlined processes and resource optimization.</h4>
                    </div>
                    <div class="d-flex align-items-center mb-4 pb-2">
                        <img src="assets/images/technology/benefits-5.svg" alt="Benefits" />
                        <h4 class="m-0 ps-4 benefits-title">Automated manual HR tasks, minimizing errors and enhancing compliance.</h4>
                    </div>
                    <div class="d-flex align-items-center mb-4 pb-2">
                        <img src="assets/images/technology/benefits-6.svg" alt="Benefits" />
                        <h4 class="m-0 ps-4 benefits-title">Enhanced workforce visibility, especially</h4>
                    </div>
                    <div class="d-flex align-items-center mb-4 pb-2">
                        <img src="assets/images/technology/benefits-7.svg" alt="Benefits" />
                        <h4 class="m-0 ps-4 benefits-title">Consolidated HR systems for a seamless user experience and accurate data.</h4>
                    </div>
                    <div class="d-flex align-items-center mb-4 pb-2">
                        <img src="assets/images/technology/benefits-8.svg" alt="Benefits" />
                        <h4 class="m-0 ps-4 benefits-title">Leveraged advanced analytics and robust reporting</h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xl-6 order-1 order-xl-2">
                <img class="mw-sm-100" src="assets/images/technology/welcome.svg" alt="Welcome">
            </div>
        </div>
        <section class="benefits-technology-partners">
            <div class="container-fluid">
                <h2 class="section-title text-center mb-4">Technology Partners</h2>
                <div class="benefits-technology-partners-slider">
                    <img src="assets/images/keka.jpg" alt="keka" />
                    <img src="assets/images/qandle.jpg" alt="qandle" />
                    <img src="assets/images/greythr.jpg" alt="qreythr" />
                    <img src="assets/images/hrone.jpg" alt="hrone" />
                    <img src="assets/images/keka.jpg" alt="keka" />
                </div>
            </div>
        </section>
    </div>
    <?php
    include('templates/contact-form.php');
    ?>
</section>
<?php
include('templates/footer.php');
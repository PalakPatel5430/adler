<?php
include('templates/header.php');
?>
<section class="inner-banner" style="background-image: url('assets/images/contact-us-inner-banner.jpg');">
    <div class="inner-banner-info">
        <h1 class="inner-banner-caption text-white">Contact Us</h1>
    </div>
</section>
<section class="inner-page contact-us-wrap">
    <div class="container">
        <div class="d-flex justify-content-between">
            <div class="inner-info">
                <h2 class="section-title">Get In Touch<span class="d-block title-border"></span></h2>
                <div class="pt-4">
                    <p>Want to take your<span class="text-primary"> HR operations to the next level? </span>Have a question, query or confusion? Get in touch with our professional team of recruiters & HR experts and make your HR operations more efficient.</p>
                    <p>Fill in the form below now</p>
                </div>
            </div>
            <div class="inner-info-pattern">
                <img class="w-100" src="assets/images/dots-pattern.png" alt="Payroll and Compliances">
            </div>
        </div>
        <div class="row">
            <div class="col-xl-8">
                <form class="get-in-touch-form pt-4">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <input type="text" class="form-control" id="companyName" placeholder="Company Name*">
                        </div>
                        <div class="col-12 col-md-6">
                            <input type="text" class="form-control" id="email" placeholder="Contact Name*">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <input type="email" class="form-control" id="emailId" placeholder="Email Id *">
                        </div>
                        <div class="col-12 col-md-6">
                            <input type="text" class="form-control" id="contactNo" placeholder="Contact No*">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <input type="text" class="form-control" id="subject" placeholder="Subject *">
                        </div>
                        <div class="col-12 col-md-6">
                            <input type="text" class="form-control" id="industry" placeholder="Industry*">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <textarea class="form-control" id="message" rows="3" placeholder="Message"></textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit<em class="icon icon-right-arrow"></em></button>
                </form>
            </div>
        </div>
        <div class="contact-info-bottom">
            <div class="row">
                <div class="col-lg-6 col-xxl-5">
                    <h2 class="section-title">Contact Info<span class="d-block title-border"></span></h2>
                    <ul class="list-unstyled contact-list">
                        <li>
                            <span class="icon">
                                <img src="assets/images/location-pin.svg" alt="Location Pin" />
                            </span>
                            <p class="m-0">Block A, 1303-1304, 13th Floor, Prahladnagar Trade Centre (PNTC), B/H Titanium City Centre, Times of India Road, Prahladnagar, Ahmedabad - 380015</p>
                        </li>
                        <li>
                            <a href="mailto:info@adlertalent.com"><span class="icon icon-mail"></span>info@adlertalent.com</a>
                        </li>
                        <li>
                            <a href="tel:+919023917973"><span class="icon icon-mobile"></span>+91 9023917973</a>
                        </li>
                    </ul>
                    <div class="d-xl-flex align-items-center">
                        <h3 class="m-0">Follow Us:</h3>
                        <ul class="list-unstyled d-flex social-list m-0">
                            <li><a href="#" class="text-decoration-none"><em class="icon icon-facebook"></em></a></li>
                            <li><a href="#" class="text-decoration-none"><em class="icon icon-instagram"></em></a></li>
                            <li><a href="#" class="text-decoration-none"><em class="icon icon-linkdin"></em></a></li>
                            <li><a href="#" class="text-decoration-none"><em class="icon icon-twitter"></em></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-xxl-7">
                    <div class="contact-map-iframe">
                        <iframe class="w-100" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3672.382016764084!2d72.51781218467865!3d23.009741663147523!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e84d792c98f53%3A0x1b0f763149f98b1!2sAdler%20Talent%20Solutions%20Pvt.%20Ltd.!5e0!3m2!1sen!2sin!4v1693786287412!5m2!1sen!2sin" width="600" height="470" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include('templates/footer.php');
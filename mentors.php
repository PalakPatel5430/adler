<?php
include('templates/header.php');
?>
<section class="inner-banner" style="background-image: url('assets/images/mentors.jpg');">
    <div class="inner-banner-info">
        <h2 class="text-white">Harness the Expertise of Mentorship <br />for Excellence </h2>
        <h1 class="inner-banner-caption text-white">HR Mentors</h1>
    </div>
</section>
<section class="inner-page mentors-wrap">
    <div class="container">
        <div class="d-flex justify-content-between">
            <div class="inner-info">
                <p>In the realm of Human Resources, <span class="text-primary">payroll management</span> is often regarded as mundane and uninspiring, leading to a lack of <span class="text-primary">innovative approaches and employer focus.</span> However, for employees, it holds immense curiosity and significance.</p>
                <p>We firmly believe that a well-designed and thoroughly understood compensation management system can distinguish companies and confer them with a strategic advantage. </p>
            </div>
            <div class="inner-info-pattern">
                <img class="w-100" src="assets/images/dots-pattern.png" alt="Payroll and Compliances">
            </div>
        </div>
    </div>
    <div class="mentors-detail-card common-card">
        <div class="container">
            <h2 class="section-title pt-5">Mentors<span class="d-block title-border"></span></h2>
            <div class="row g-4 justify-content-center mentors-list">
                <div class="col-sm-6 col-lg-4 col-xl-3">
                    <div class="card">
                        <img class="w-100 radius-8" src="assets/images/team-member-1.jpg" alt="Team" />
                        <div class="card-info-wrap">
                            <div class="card-info">
                                <div class="two-line-ellipsis mt-2">
                                    <h3 class="font-30">Vijay Kondiram Shitole</h3>
                                </div>
                                <p class="m-0"><strong>36</strong> years of experience</p>
                            </div>
                            <hr />
                            <a href="#" class="btn btn-link p-0 text-decoration-none text-capitalize"><span>Linkdin</span> <em class="icon icon-up-arrow"></em></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 col-xl-3">
                    <div class="card">
                        <img class="w-100 radius-8" src="assets/images/team-member-2.jpg" alt="Team" />
                        <div class="card-info-wrap">
                            <div class="card-info">
                                <div class="two-line-ellipsis mt-2">
                                    <h3 class="font-30">Rohit Joshi</h3>
                                </div>
                                <p class="m-0"><strong>21</strong> years of experience</p>
                            </div>
                            <hr />
                            <a href="#" class="btn btn-link p-0 text-decoration-none text-capitalize"><span>Linkdin</span> <em class="icon icon-up-arrow"></em></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 col-xl-3">
                    <div class="card">
                        <img class="w-100 radius-8" src="assets/images/team-member-3.jpg" alt="Team" />
                        <div class="card-info-wrap">
                            <div class="card-info">
                                <div class="two-line-ellipsis mt-2">
                                    <h3 class="font-30">Sumit Raisinghani </h3>
                                </div>
                                <p class="m-0"><strong>17</strong> years of experience</p>
                            </div>
                            <hr />
                            <a href="#" class="btn btn-link p-0 text-decoration-none text-capitalize"><span>Linkdin</span> <em class="icon icon-up-arrow"></em></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 col-xl-3">
                    <div class="card">
                        <img class="w-100 radius-8" src="assets/images/team-member-4.jpg" alt="Team" />
                        <div class="card-info-wrap">
                            <div class="card-info">
                                <div class="two-line-ellipsis mt-2">
                                    <h3 class="font-30">Sanjay Mandlik </h3>
                                </div>
                                <p class="m-0"><strong>32</strong> years of experience</p>
                            </div>
                            <hr />
                            <a href="#" class="btn btn-link p-0 text-decoration-none text-capitalize"><span>Linkdin</span> <em class="icon icon-up-arrow"></em></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 col-xl-3">
                    <div class="card">
                        <img class="w-100 radius-8" src="assets/images/team-member-1.jpg" alt="Team" />
                        <div class="card-info-wrap">
                            <div class="card-info">
                                <div class="two-line-ellipsis mt-2">
                                    <h3 class="font-30">Vijay Kondiram Shitole</h3>
                                </div>
                                <p class="m-0"><strong>36</strong> years of experience</p>
                            </div>
                            <hr />
                            <a href="#" class="btn btn-link p-0 text-decoration-none text-capitalize"><span>Linkdin</span> <em class="icon icon-up-arrow"></em></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 col-xl-3">
                    <div class="card">
                        <img class="w-100 radius-8" src="assets/images/team-member-2.jpg" alt="Team" />
                        <div class="card-info-wrap">
                            <div class="card-info">
                                <div class="two-line-ellipsis mt-2">
                                    <h3 class="font-30">Rohit Joshi</h3>
                                </div>
                                <p class="m-0"><strong>21</strong> years of experience</p>
                            </div>
                            <hr />
                            <a href="#" class="btn btn-link p-0 text-decoration-none text-capitalize"><span>Linkdin</span> <em class="icon icon-up-arrow"></em></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 col-xl-3">
                    <div class="card">
                        <img class="w-100 radius-8" src="assets/images/team-member-3.jpg" alt="Team" />
                        <div class="card-info-wrap">
                            <div class="card-info">
                                <div class="two-line-ellipsis mt-2">
                                    <h3 class="font-30">Sumit Raisinghani </h3>
                                </div>
                                <p class="m-0"><strong>17</strong> years of experience</p>
                            </div>
                            <hr />
                            <a href="#" class="btn btn-link p-0 text-decoration-none text-capitalize"><span>Linkdin</span> <em class="icon icon-up-arrow"></em></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 col-xl-3">
                    <div class="card">
                        <img class="w-100 radius-8" src="assets/images/team-member-4.jpg" alt="Team" />
                        <div class="card-info-wrap">
                            <div class="card-info">
                                <div class="two-line-ellipsis mt-2">
                                    <h3 class="font-30">Sanjay Mandlik </h3>
                                </div>
                                <p class="m-0"><strong>32</strong> years of experience</p>
                            </div>
                            <hr />
                            <a href="#" class="btn btn-link p-0 text-decoration-none text-capitalize"><span>Linkdin</span> <em class="icon icon-up-arrow"></em></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <?php
    include('templates/contact-form.php');
    ?>
</section>
<?php
include('templates/footer.php');
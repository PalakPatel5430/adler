<?php
include('templates/header.php');
?>
<section class="inner-banner" style="background-image: url('assets/images/why-us/why-us.jpg');">
    <div class="inner-banner-info">
        <h1 class="inner-banner-caption text-white">Why Us</h1>
    </div>
</section>
<section class="inner-page why-us-wrap">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xl-6 order-2 order-xl-1">
                <div class="welcome-info pt-4 pt-xl-0">
                    <p>At ThinkHR, we pride ourselves on being the <span class="text-primary">go-to destination for all your HR needs.</span> Our team is driven by results, with a meticulous attention to detail and a versatile approach that ensures we meet and exceed your expectations. We understand the importance of talent and leverage icebreakers, resilience, and creativity to unlock the full potential of your workforce.</p>
                    <p>Embracing technology, we adopt a system-driven approach that streamlines operations, making your HR processes efficient and hassle-free. Our team of gearheads is always one step ahead, staying abreast of industry trends to provide innovative solutions that keep your business ahead of the curve.</p>
                    <p>Trust is the foundation of our relationship with clients. We operate with unwavering integrity, transparency, and credence, ensuring that your business interests are always protected. Our deep understanding of diverse industries enables us to offer tailor-made solutions that align perfectly with your unique business requirements.</p>
                    <p>At Think HR, we embody a next-gen approach, embracing the dynamic collaboration of Gen X, millennials, and Gen Z. This fusion of experience, innovation, and creativity fuels our ability to deliver solutions that cater to startups, unicorns, and high-end corporates alike.</p>
                </div>
                <h2 class="section-title">Values<span class="d-block title-border"></span></h2>
                <div class="values-list">
                    <div class="values-box d-sm-flex align-items-start">
                        <div class="values-img">
                            <img src="assets/images/why-us/values-1.svg" alt="Values" />
                        </div>
                        <div>
                            <h6><strong>Team: Highly Experienced and Completely Professional</strong></h6>
                            <p>Our team of HR consultants is comprised of highly committed professionals with <strong class="text-primary">extensive experience in handling dynamic headhunting and HR advisory requirements</strong> of modern organizations. With a global footprint spanning various industry segments, our experts meticulously analyze your needs and select the perfect resource to meet your company's demands.</p>
                        </div>
                    </div>
                    <div class="values-box d-sm-flex align-items-start">
                        <div class="values-img">
                            <img src="assets/images/why-us/values-2.svg" alt="Values" />
                        </div>
                        <div>
                            <h6><strong>Technology: Personalized Solutions Powered by Advanced Technology</strong></h6>
                            <p>ThinkHR, we embrace cutting-edge technology to deliver personalized solutions that drive efficiency throughout the HR value chain. Our <strong class="text-primary">fully digitized processes</strong>ensure swift responses, end-to-end support, and an exceptional engagement experience that consistently surpasses expectations.</p>
                        </div>
                    </div>
                    <div class="values-box d-sm-flex align-items-start">
                        <div class="values-img">
                            <img src="assets/images/why-us/values-3.svg" alt="Values" />
                        </div>
                        <div>
                            <h6><strong>Trust: Reliability that Fuels Organizational Growth</strong></h6>
                            <p>Transparency lies at the core of our organizational values, allowing us to establish a commendable rapport with all our clients. By upholding the highest standards of integrity in our policies and procedures, we aim to provide reliable HR solutions that cater to diverse industry sectors. Our <strong class="text-primary">unwavering commitment to trust</strong>enhances your organization's potential for growth.</p>
                        </div>
                    </div>
                    <div class="values-box d-sm-flex align-items-start">
                        <div class="values-img">
                            <img src="assets/images/why-us/values-4.svg" alt="Values" />
                        </div>
                        <div>
                            <h6><strong>Talent: A Vast Pool of Expertise Spanning Industry Segments</strong></h6>
                            <p>ThinkHR has earned a reputation for successfully <strong class="text-primary">addressing the HR needs of global organizations</strong> operating in diverse sectors such as manufacturing, engineering, ports, logistics, pharmaceuticals, travel, IT, retail, eCommerce, and more. Our extensive network has allowed us to cultivate a substantial talent pool, enabling us to swiftly identify and connect your organization with highly skilled professionals who possess the specific expertise you require.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xl-6 order-1 order-xl-2">
                <img class="mw-sm-100" src="assets/images/why-us/why-us-right-bar.svg" alt="Welcome">
            </div>
        </div>
    </div>
    <?php
    include('templates/contact-form.php');
    ?>
</section>
<?php
include('templates/footer.php');
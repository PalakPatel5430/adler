<!-- Header -->
<?php
include('templates/header.php');
?>
<!-- Home Banner -->
<section class="home-banner">
    <div class="video position-relative player-controls">
        <video id="ban_video" class="tv_video position-relative">
            <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4"> Your browser does not support the video tag.
        </video>
        <div class="play-bt"> <em class="icon icon-play"></em></div>
        <div class="pause-bt" style="display:none;"><em class="icon icon-pause"></em></div>
        <!-- <div class="mute-bt"></div>
        <div class="stop-bt"></div> -->
    </div>
</section>
<!-- Think HR : START -->
<section class="think-hr-wrap">
    <h1 class="text-end think-caption section-title">The Core of Think HR</h1>
    <div class="think-hr-slider">
        <div class="card">
            <img src="assets/images/think-hr-1.jpg" alt="HR Services" />
            <h2 class="card-title"><a href="#">HR Services</a></h2>
            <div class="card-info">
                <p>Our comprehensive HR services wave their wand to ensure smooth and efficient management of your workforce.</p>
            </div>
            <hr />
            <a href="#" class="btn btn-link p-0 text-decoration-none"><span>Read More</span> <em class="icon icon-up-arrow"></em></a>
        </div>
        <div class="card">
            <img src="assets/images/think-hr-2.jpg" alt="HR Services" />
            <h2 class="card-title"><a href="#">HR Technology</a></h2>
            <div class="card-info">
                <p>In a world where you are always in a battle with the ticking clock, our dynamic alliances with 20+ leading HR tech providers come to the rescue.</p>
            </div>
            <hr />
            <a href="#" class="btn btn-link p-0 text-decoration-none"><span>Read More</span> <em class="icon icon-up-arrow"></em></a>
        </div>
        <div class="card">
            <img src="assets/images/think-hr-3.jpg" alt="HR Services" />
            <h2 class="card-title"><a href="#">Mentors</a></h2>
            <div class="card-info">
                <p>Ditch the average, and tap into the exceptional. We've teamed up with a league of extraordinary mentors who bring their A-game to the table.</p>
            </div>
            <hr />
            <a href="#" class="btn btn-link p-0 text-decoration-none"><span>Read More</span> <em class="icon icon-up-arrow"></em></a>
        </div>
    </div>
</section>
<!-- Think HR : About Us -->
<section class="about-us">
    <div class="containter-fluid">
        <div class="row">
            <div class="col-12 col-lg-5 order-2 order-lg-1 pt-4 pt-lg-0">
                <h2 class="about-title section-title">About Us</h2>
                <h3 class="about-sub-title section-sub-title">Transforming the HR Solutions with Unparalleled Expertise and Innovation</h3>
                <div class="about-info">
                    <p>Harnessing the unstoppable prowess of Adler Talent Solutions, one of India's premier Strategic HR companies, we're set to rock the HR scene like never before. Armed with our expertise in HR services, HR technology, and HR mentoring, we'll uncover the perfect HR solution for your organization while flawlessly handling all your mundane HR tasks.</p>
                </div>
                <a href="#" class="btn btn-link p-0 text-decoration-none"><span>Read More</span> <em class="icon icon-up-arrow"></em></a>
            </div>
            <div class="col-12 col-lg-7 order-1 order-lg-2">
                <div class="about-us-img-wrap">
                    <img src="assets/images/about-us.jpg" alt="About Us" />
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Company's achievement : START -->
<section class="company-achievement">
    <div class="achievement-grid">
        <div class="inner-col">
            <div class="digit-wrap">
                <h6 class="digit">30+</h6>
                <p>HR Services offered</p>
            </div>
        </div>
        <div class="inner-col">
            <div class="digit-wrap">
                <h6 class="digit">3200+</h6>
                <p>Employees Managed</p>
            </div>
        </div>
        <div class="inner-col">
            <div class="digit-wrap">
                <h6 class="digit">11</h6>
                <p>Domestic Geographies</p>
            </div>
        </div>
        <div class="inner-col">
            <div class="digit-wrap">
                <h6 class="digit">3</h6>
                <p>International Geographies</p>
            </div>
        </div>
        <div class="inner-col">
            <div class="digit-wrap">
                <h6 class="digit">7</h6>
                <p>Mentors</p>
            </div>
        </div>
        <div class="inner-col">
            <div class="digit-wrap">
                <h6 class="digit">10</h6>
                <p>Tech Partners</p>
            </div>
        </div>
    </div>
    <span class="rounded-circle circle-orange circle-orange-sm"></span>
    <span class="rounded-circle circle-sacer circle-sacer-sm"></span>
    <span class="rounded-circle circle-sacer circle-sacer-md"></span>
    <span class="rounded-circle circle-orange circle-orange-xl"></span>
    <span class="rounded-circle circle-orange circle-orange-md"></span>
    <div class="get-in-touch">
        <div class="d-sm-flex justify-content-center align-items-center">
            <h2 class="get-in-touch-title">Get in Touch with Us for HR Solutions That Drive Success!</h2>
            <a href="#" class="btn btn-primary bg-white text-primary">Contact Us<em class="icon icon-right-arrow"></em></a>
        </div>
    </div>
</section>
<!-- Technology Partners : START -->
<section class="technology-partners">
    <div class="container-fluid">
        <h2 class="section-title text-center">Technology Partners</h2>
        <div class="technology-partners-slider">
            <img src="assets/images/keka.jpg" alt="keka" />
            <img src="assets/images/qandle.jpg" alt="qandle" />
            <img src="assets/images/greythr.jpg" alt="qreythr" />
            <img src="assets/images/hrone.jpg" alt="hrone" />
            <img src="assets/images/keka.jpg" alt="keka" />
        </div>
    </div>
</section>
<!-- Client Testimonial : START -->
<section class="client-testimonial">
    <div class="client-testimonial-slider">
        <div class="row d-flex">
            <div class="col-md-6 col-xl-4">
                <div class="video position-relative player-controls">
                    <video id="testimonailVideo" class="testimonail-video position-relative w-100">
                        <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4"> Your browser does not support the video tag.
                    </video>
                    <div class="play-bt"> <em class="icon icon-play"></em></div>
                    <div class="pause-bt" style="display:none;"><em class="icon icon-pause"></em></div>
                    <!-- <div class="mute-bt"></div>
                    <div class="stop-bt"></div> -->
                </div>
            </div>
            <div class="col-md-6 col-xl-8">
                <div class="row">
                    <div class="col-md-12 col-xl-7">
                        <div class="testimonial-info-wrap">
                            <h2 class="text-white section-title">Client Testimonial</h2>
                            <div class="testimonial-info">
                                <em class="icon-testimonial-up testimonial-quote pb-3 d-block"></em>
                                <p class="text-white font-25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                <span class="testimonail-author font-25 text-white d-block">Lorem Ipsum</span>
                                <em class="icon-testimonial-down testimonial-quote pt-3 d-block"></em>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xl-5">
                        <div class="testimonial-author-img">
                            <img src="assets/images/testimonial.jpg" alt="Testimonial" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-flex">
            <div class="col-md-6 col-xl-4">
                <div class="video position-relative player-controls">
                    <video id="testimonailVideo" class="testimonail-video position-relative w-100">
                        <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4"> Your browser does not support the video tag.
                    </video>
                    <div class="play-bt"> <em class="icon icon-play"></em></div>
                    <div class="pause-bt" style="display:none;"><em class="icon icon-pause"></em></div>
                    <!-- <div class="mute-bt"></div>
                    <div class="stop-bt"></div> -->
                </div>
            </div>
            <div class="col-md-6 col-xl-8">
                <div class="row">
                    <div class="col-md-12 col-xl-7">
                        <div class="testimonial-info-wrap">
                            <h2 class="text-white section-title">Client Testimonial</h2>
                            <div class="testimonial-info">
                                <em class="icon-testimonial-up testimonial-quote pb-3 d-block"></em>
                                <p class="text-white font-25">Consultation, Leadership, Mentoring session with clients</p>
                                <span class="testimonail-author font-25 text-white d-block">Lorem Ipsum</span>
                                <em class="icon-testimonial-down testimonial-quote pt-3 d-block"></em>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xl-5">
                        <div class="testimonial-author-img">
                            <img src="assets/images/testimonial.jpg" alt="Testimonial" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<!-- Blogs : START -->
<section class="blogs-wrap">
    <div class="container-fluid">
        <div class="text-center">
            <h2 class="section-title">Blogs</h2>
            <h3 class="section-sub-title">Dive into the world of<span class="d-block"> HR Solutions</span></h3>
        </div>
        <div class="row">
            <div class="col-md-9 pe-0">
                <div class="blogs-slider common-card">
                    <div class="card">
                        <img class="w-100" src="assets/images/blogs-1.jpg" alt="Blogs" />
                        <div class="card-info-wrap">
                            <div class="card-info">
                                <p>Top influencers from all across the globe</p>
                            </div>
                            <hr />
                            <a href="#" class="btn btn-link p-0 text-decoration-none"><span>Read More</span> <em class="icon icon-up-arrow"></em></a>
                        </div>
                    </div>
                    <div class="card">
                        <img class="w-100" src="assets/images/blogs-2.jpg" alt="Blogs" />
                        <div class="card-info-wrap">
                            <div class="card-info">
                                <p>LinkedIn marketing strategist sharing their insights</p>
                            </div>
                            <hr />
                            <a href="#" class="btn btn-link p-0 text-decoration-none"><span>Read More</span> <em class="icon icon-up-arrow"></em></a>
                        </div>
                    </div>
                    <div class="card">
                        <img class="w-100" src="assets/images/blogs-3.jpg" alt="Blogs" />
                        <div class="card-info-wrap">
                            <div class="card-info">
                                <p>Consultation, Leadership, Mentoring session with clients</p>
                            </div>
                            <hr />
                            <a href="#" class="btn btn-link p-0 text-decoration-none"><span>Read More</span> <em class="icon icon-up-arrow"></em></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 p-0">
                <div class="view-more-with-pattern">
                    <img src="assets/images/dots-pattern.png" />
                    <a href="#" class="btn btn-link btn-link-white p-0 text-decoration-none text-capitalize"><span>View More</span> <em class="icon icon-up-arrow"></em></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Footer -->
<?php

include('templates/footer.php');
?>

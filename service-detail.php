<?php
include('templates/header.php');
?>
<section class="inner-banner" style="background-image: url('assets/images/HR-Strategy.jpg');">
    <div class="inner-banner-info">
        <h2 class="text-white">Unlock Payroll Peace and Compliance <br /> Confidence with Our Tailored Solutions</h2>
        <h1 class="inner-banner-caption text-white">Payroll and Compliances</h1>
        <!-- <div class="breadcrumbs">
            <ul>
                <li><a href="#"><span class="border-white"></span>Home</a></li>
                <li><a href="#"><em class="icon icon-play-breadcum"></em>About</a></li>
                <li>About</li>
            </ul>
        </div> -->
    </div>
</section>
<section class="inner-page service-detail-wrap">
    <div class="container">
        <div class="d-flex justify-content-between">
            <div class="inner-info">
                <p>In the realm of Human Resources, <span class="text-primary">payroll management</span> is often regarded as mundane and uninspiring, leading to a lack of <span class="text-primary">innovative approaches and employer focus.</span> However, for employees, it holds immense curiosity and significance.</p>
                <p>We firmly believe that a well-designed and thoroughly understood compensation management system can distinguish companies and confer them with a strategic advantage. </p>
            </div>
            <div class="inner-info-pattern">
                <img class="w-100" src="assets/images/dots-pattern.png" alt="Payroll and Compliances">
            </div>
        </div>
    </div>
    <div class="service-detail-card common-card">
        <div class="container">
            <div class="row g-4 justify-content-center">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-info-wrap">
                            <img src="assets/images/payroll-1.svg" alt="Payroll" />
                            <div class="two-line-ellipsis mt-2">
                                <h4 class="card-caption"><a class="text-decoration-none text-black" href="#">Compensation & structure - designing & implementation</a></h4>
                            </div>
                            <div class="five-line-ellipsis card-info">
                                <p>Our Compensation & Structure service offers comprehensive assistance in designing and implementing effective compensation strategies and organizational structures.</p>
                            </div>
                            <a href="#" class="btn btn-link p-0 text-decoration-none" tabindex="0"><span>Read More</span> <em class="icon icon-up-arrow"></em></a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-info-wrap">
                            <img src="assets/images/payroll-2.svg" alt="Payroll" />
                            <div class="two-line-ellipsis mt-2">
                                <h4 class="card-caption"><a class="text-decoration-none text-black" href="#">Payroll & Compliance Management</a></h4>
                            </div>
                            <div class="five-line-ellipsis card-info">
                                <p>We prioritise streamlining your payroll procedures and safeguarding compliance to ensure you enjoy complete peace of mind.</p>
                            </div>
                            <a href="#" class="btn btn-link p-0 text-decoration-none" tabindex="0"><span>Read More</span> <em class="icon icon-up-arrow"></em></a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-info-wrap">
                            <img src="assets/images/payroll-3.svg" alt="Payroll" />
                            <div class="two-line-ellipsis mt-2">
                                <h4 class="card-caption"><a class="text-decoration-none text-black" href="#">New Wage Code Implementation</a></h4>
                            </div>
                            <div class="five-line-ellipsis card-info">
                                <p>Our innovative New Wage Code Implementation services – designed to provide organizations with efficient and effective transitioning to the revised wage regulations.</p>
                            </div>
                            <a href="#" class="btn btn-link p-0 text-decoration-none" tabindex="0"><span>Read More</span> <em class="icon icon-up-arrow"></em></a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-info-wrap">
                            <img src="assets/images/payroll-4.svg" alt="Payroll" />
                            <div class="two-line-ellipsis mt-2">
                                <h4 class="card-caption"><a class="text-decoration-none text-black" href="#">Industrial Relations</a></h4>
                            </div>
                            <div class="five-line-ellipsis card-info">
                                <p>We recognize the importance of fostering harmonious workplace relationships for successful organizational growth.</p>
                            </div>
                            <a href="#" class="btn btn-link p-0 text-decoration-none" tabindex="0"><span>Read More</span> <em class="icon icon-up-arrow"></em></a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-info-wrap">
                            <img src="assets/images/payroll-5.svg" alt="Payroll" />
                            <div class="two-line-ellipsis mt-2">
                                <h4 class="card-caption"><a class="text-decoration-none text-black" href="#">Statutory Audit</a></h4>
                            </div>
                            <div class="five-line-ellipsis card-info">
                                <p>We understand how vital it is for your industry to adhere to statutory regulations and to provide precise financial reporting.</p>
                            </div>
                            <a href="#" class="btn btn-link p-0 text-decoration-none" tabindex="0"><span>Read More</span> <em class="icon icon-up-arrow"></em></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    include('templates/contact-form.php');
    ?>
</section>
<?php
include('templates/footer.php');
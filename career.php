<?php
include('templates/header.php');
?>
<section class="inner-banner" style="background-image: url('assets/images/career/career.jpg');">
    <div class="inner-banner-info">
        <h1 class="inner-banner-caption text-white">Career</h1>
    </div>
</section>
<section class="inner-page employee-testimonial-wrap">
    <div class="container">
        <div class="d-flex justify-content-between">
            <div class="inner-info pe-4">
                <p>Our exceptional culture is the <span class="text-primary">cornerstone of our strategic HR excellence.</span> Engage in monthly activities that promote teamwork and creativity. Bask in the recognition through our R&R initiatives that highlight your contributions. Whether it's celebrating your milestones, embarking on trips, or enjoying casual work attire, we ensure your comfort and joy. With monthly training sessions, we empower your growth. Come, be a part of our dynamic family, and experience a nurturing culture that resonates with your aspirations.</p>
            </div>
            <div class="inner-info-pattern">
                <img class="w-100" src="assets/images/dots-pattern.png" alt="Payroll and Compliances">
            </div>
        </div>
    </div>
    <div class="employee-detail-card common-card">
        <div class="container">
            <h2 class="section-title pt-5">Employee Testimonial<span class="d-block title-border"></span></h2>
            <div class="employee-testimonial-slider common-card">
                <div class="card">
                    <img class="w-100 radius-60" src="assets/images/team-member-1.jpg" alt="Team" />
                    <div class="card-info-wrap">
                        <div class="card-info">
                            <h3 class="font-30">Thomas Novak</h3>
                            <p class="m-0">Chief Manager</p>
                        </div>
                        <div class="member-description mt-4">
                            <p>I am delighted to share my testimonial for ThinkHR, the company I have had the privilege of working with for the last couple of years. When I joined ThinkHR, little did I know that it would become a transformative journey, shaping me both personally and professionally. From the very beginning, ThinkHR provided me with an environment that fostered growth and learning. </p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <img class="w-100 radius-60" src="assets/images/team-member-2.jpg" alt="Team" />
                    <div class="card-info-wrap">
                        <div class="card-info">
                            <h3 class="font-30">Amanda Berry</h3>
                            <p class="m-0">Managing Director</p>
                        </div>
                        <div class="member-description mt-4">
                            <p>I am delighted to share my testimonial for ThinkHR, the company I have had the privilege of working with for the last couple of years. When I joined ThinkHR, little did I know that it would become a transformative journey, shaping me both personally and professionally. From the very beginning, ThinkHR provided me with an environment that fostered growth and learning. </p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <img class="w-100 radius-60" src="assets/images/team-member-3.jpg" alt="Team" />
                    <div class="card-info-wrap">
                        <div class="card-info">
                            <h3 class="font-30">Bhagyashree Pachegaonkar</h3>
                            <p class="m-0">Managing Director</p>
                        </div>
                        <div class="member-description mt-4">
                            <p>The organisation has always stood tall on the central values of transparency and integrity which has always kept me engaged to develop myself professionally. The organisation reflects a goal oriented approach in all its processes and its approaches where the external clients as well as internal stakeholders always get clear direction - this aspect of the culture has given a clear-cut path to me where I could grow from an intern level to a Leader level with constant guidance & rigorous training at each level.</p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <img class="w-100 radius-60" src="assets/images/team-member-3.jpg" alt="Team" />
                    <div class="card-info-wrap">
                        <div class="card-info">
                            <h3 class="font-30">Bhagyashree Pachegaonkar</h3>
                            <p class="m-0">Managing Director</p>
                        </div>
                        <div class="member-description mt-4">
                            <p>The organisation has always stood tall on the central values of transparency and integrity which has always kept me engaged to develop myself professionally. The organisation reflects a goal oriented approach in all its processes and its approaches where the external clients as well as internal stakeholders always get clear direction - this aspect of the culture has given a clear-cut path to me where I could grow from an intern level to a Leader level with constant guidance & rigorous training at each level.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    <?php
    include('templates/contact-form.php');
    ?>
</section>
<?php
include('templates/footer.php');